package com.univlille.testrxzip;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

/**
 * Petit exemple qui montre comment 'fusionner' deux Observables pour produire un nouvel Observable.
 * - Merge fusionne simplement les deux flux.
 * - ZipWith crée un nouvel Observable pour lequel on peut appliquer une fonction sur chaque élément résultatnt de la fusion.
 *
 * Pour rappel, il y a beaucoup d'autres opérateurs pour Rx, cf. https://reactivex.io/documentation/operators.html
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        testFusionFluxRx();
    }

    private void testFusionFluxRx() {
        Observable<String> observable1 = Observable.just("a", "b", "c", "d");
        Observable<String> observable2 = Observable.just("x", "y", "z");

        // fusion 'simple' de deux flux. Le résultat (avec l'observer) sera :
        //      next resultatMerge : a
        //      next resultatMerge : b
        //      next resultatMerge : c
        //      next resultatMerge : d
        //      next resultatMerge : x
        //      next resultatMerge : y
        //      next resultatMerge : z
        Observable<String> merge = Observable.merge(observable1, observable2);

        // fusion de deux flux avec fonction de traitement sur le nouveau flux produit.
        // Pour cet exemple, la fonction  met en majuscule les éléments du flux 1 qui sont < "c",
        // et concatène les deux flux.
        // --> résultat :
        //      next zipWith : Ax
        //      next zipWith : By
        //      next zipWith : cz
        Observable<String> zipWith = observable2.zipWith(observable1, new BiFunction<String, String, String>() {
            @NonNull
            @Override
            public String apply(@NonNull String s_observable2, @NonNull String s_observable1) throws Exception {
                if (s_observable1.compareTo("c") < 0) {
                    s_observable1 = s_observable1.toUpperCase();
                }
                return s_observable1 + s_observable2;
            }
        });


        Observer<String> resultatMerge = merge.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        Log.d("JC", "subscribe resultatMerge OK");
                    }

                    @Override
                    public void onNext(@NonNull String s) {
                        Log.d("JC", "next resultatMerge : " + s);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d("JC", "error resultatMerge : " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("JC", "complete resultatMerge");
                    }
                });

        Observer<String> resultatZipWith = zipWith.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<String>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        Log.d("JC", "subscribe zipWith OK");
                    }

                    @Override
                    public void onNext(@NonNull String s) {
                        Log.d("JC", "next zipWith : " + s);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d("JC", "error zipWith : " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.d("JC", "complete zipWith");
                    }
                });

//        Observer<String> observer2 = observable2.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new Observer<String>() {
//                    @Override
//                    public void onSubscribe(@NonNull Disposable d) {
//                        Log.d("JC", "subscribe 2 OK");
//                    }
//
//                    @Override
//                    public void onNext(@NonNull String s) {
//                        Log.d("JC", "next subscribe 2 : " + s);
//                    }
//
//                    @Override
//                    public void onError(@NonNull Throwable e) {
//                        Log.d("JC", "error subscribe 2 : " + e.getMessage());
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        Log.d("JC", "complete subscribe 2");
//                    }
//                });


    }
}